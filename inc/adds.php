<?php
/**
 * Logic for display adds from options page.
 */

// get banners and links for each adds
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

//function for view
function banner_view($numb,$objadd) {

	$content = '
	<div class="adds adds__'.$numb.'">
	<a href="'.$objadd->adds_link.' " target="_blank" rel="noopener noreferrer">
	<picture>
		<source media="(min-width: 1530px)" srcset="'. $objadd->list_frm['adds_banner' . $numb . 'format_1'] .'">
		<source media="(min-width: 1024px)" srcset="'. $objadd->list_frm['adds_banner' . $numb . 'format_2'] .'">
		<source media="(min-width: 768px)"  srcset="'. $objadd->list_frm['adds_banner' . $numb . 'format_3'] .'">
		<source media="(min-width: 376px)"  srcset="'. $objadd->list_frm['adds_banner' . $numb . 'format_4'] .'">
		<img class="adds_banner adds_banner__'.$numb.'" src="'.$objadd->list_frm['adds_banner' . $numb . 'format_5'].'" alt="'.$objadd->list_frm['adds_banner' . $numb . 'alt_1'].'">
	</picture>
	</a>
	</div>
	';

	return $content;
}

//function for dynamic getting variables
function get_variables($numb) {

	if (class_exists('ACF') && have_rows('adds_'.$numb, 'options')) {
		while (have_rows('adds_'.$numb, 'options')) {
			// var_dump(the_row());
			the_row();

			$adds_object = new \stdClass;

			if ($adds_url = get_sub_field('adds_url', 'options')) {

				$adds_url = esc_url($adds_url);
				$adds_object->adds_link=$adds_url;
			}
			else {
				$adds_object->adds_link='';
			}

			// get all banner formats. 5 is the number of banners types.
			$banners_type = 5;

			for($i=1; $i<=$banners_type; $i++) {

				${'banner_format_'.$i} = get_sub_field( 'banner_format_'.$i, 'options' );

				if ( ${'banner_format_'.$i} ) {
					$adds_object->list_frm['adds_banner' . $numb . 'format_' . $i] = ${'banner_format_'.$i}['url'];
					$adds_object->list_frm['adds_banner' . $numb . 'alt_' . $i]	= ${'banner_format_'.$i}['alt'];
				}
				else {
					$adds_object->list_frm['adds_banner' . $numb . 'format_' . $i] = '';
					$adds_object->list_frm['adds_banner' . $numb . 'alt_' . $i]	= '';
				}
			}
			$list_of_objects=$adds_object;
		}
	}

	return $list_of_objects;
}

//create shortcodes
function display_adds_banner( $atts ){

extract(shortcode_atts(array(
	'baner' => 1,
	), $atts));

$numb = $baner;
$objadd = get_variables($numb);
$content = banner_view($numb,$objadd);

return $content;
}

add_shortcode( 'reklama', 'display_adds_banner' );
?>