<?php
/**
 * Logic for display additional code in header and footer.  
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// headrer
 if ( $header_code = get_field( 'header_code', 'options' ) ) :
    
    function cs_add_code_header() {
        $header_code = get_field( 'header_code', 'options' );
        echo $header_code;
    }

    add_action('wp_head' , 'cs_add_code_header', 10, 1);
 endif;

 // footer
 if ( $footer_code = get_field( 'footer_code', 'options' ) ) :  
    
    function cs_add_code_footer() {
        $footer_code = get_field( 'footer_code', 'options' );
        echo $footer_code;
    }

    add_action('wp_footer' , 'cs_add_code_footer', 30, 1);
 endif;

?>