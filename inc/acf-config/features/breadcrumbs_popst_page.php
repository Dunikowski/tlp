<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_60abad0bd8e29',
        'title' => 'Okruszki dla wpisów i stron',
        'fields' => array(
            array(
                'key' => 'field_60abad17a3de0',
                'label' => 'Ukryć okruszki na tej stronie?',
                'name' => 'hide_breadcrumbs',
                'type' => 'true_false',
                'instructions' => 'Jeśli w systemie została aktywowana funkcja wyświetlania okruszków tutaj możesz wyłączyć ich wyświetlanie dla tej strony/wpisu.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 10,
        'position' => 'side',
        'style' => 'default closed',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
?>