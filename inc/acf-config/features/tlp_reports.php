<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5d935e5024de4',
        'title' => 'Raporty',
        'fields' => array(
            array(
                'key' => 'field_58b56f5a5f8e8',
                'label' => 'Ścieżka do pliku',
                'name' => 'sciezka_do_pliku',
                'type' => 'file',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'library' => 'all',
                'return_format' => 'array',
                'min_size' => 0,
                'max_size' => 0,
                'mime_types' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'seamless',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
        ),
        'active' => true,
        'description' => '',
    ));
    
    endif;
?>