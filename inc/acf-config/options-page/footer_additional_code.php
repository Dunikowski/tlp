<?php 
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6086d29cbfd43',
        'title' => 'Kod w stopce strony',
        'fields' => array(
            array(
                'key' => 'field_6086d2adef5ec',
                'label' => 'Kod w stopce',
                'name' => 'footer_code',
                'type' => 'wysiwyg',
                'instructions' => 'Tutaj możesz umieścić dowolny kod np. JavaScript aby został wywołany na końcu strony, przed zamknięciem znacznika body.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'text',
                'media_upload' => 0,
                'toolbar' => 'full',
                'delay' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-stopka',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default closed',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
?>