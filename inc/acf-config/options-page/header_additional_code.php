<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6086d196c2479',
        'title' => 'Kod w sekcji nagłówka',
        'fields' => array(
            array(
                'key' => 'field_6086d1a67da9f',
                'label' => 'Dodatkowy kod w nagłówku',
                'name' => 'header_code',
                'type' => 'wysiwyg',
                'instructions' => 'Tutaj możesz wkleić dowolny kod, który zostanie umieszczony na początku strony w sekcji nagłówka (przed zamknięciem znacznika head).',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'text',
                'media_upload' => 0,
                'toolbar' => 'full',
                'delay' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-naglowek',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default closed',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
?>