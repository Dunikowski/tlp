<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_60ab970f0af19',
        'title' => 'Okruszki',
        'fields' => array(
            array(
                'key' => 'field_60ab972ce06b1',
                'label' => 'Wyświetlać okruszki?',
                'name' => 'bradcrumbs',
                'type' => 'true_false',
                'instructions' => 'Włącz używanie okruszków w całym serwisie. Gdy ta opcja jest włączona możesz wyłączyć wyświetlanie okruszków na poszczególnych podstronach. Jeśli ta opcja jest wyłączona, okruszki nie będą w ogóle wyświetlane.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'tlp-general-settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default closed',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
?>