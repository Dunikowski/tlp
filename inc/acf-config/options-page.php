<?php
//create page options - require using ACF PRO plugin with all options used inside

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Opcje',
		'menu_title'	=> 'TLP - Ustawienia',
		'menu_slug' 	=> 'tlp-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Ustawienia Nagłówka Motywu',
		'menu_title'	=> 'Naglówek',
		'parent_slug'	=> 'tlp-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Ustawienia Stopki Motywu',
		'menu_title'	=> 'Stopka',
		'parent_slug'	=> 'tlp-general-settings',
	));	
}
?>