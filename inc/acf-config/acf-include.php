<?php

// set scheme path
$feature_path = get_template_directory() . '/inc/acf-config/';

// create options page
require $feature_path . 'options-page.php';

//include code functions previously created in plugin settings
/**
 * For each setting you create, include PHP source code and add the source settings in the json file. 
 * If you want to change something, import the setting via json, comment the code line below responsible 
 * for adding this function from the code, and feel free to edit with ACF plugin settings. Once you are 
 * done making changes as above, include the changes in the php and json files here.
 */
// for options page
require $feature_path . 'options-page/header_additional_code.php';
require $feature_path . 'options-page/footer_additional_code.php';
require $feature_path . 'options-page/tlp_logos.php';
require $feature_path . 'options-page/tlp_adds.php';
require $feature_path . 'options-page/password_protect.php';
require $feature_path . 'options-page/tlp_popup.php';
require $feature_path . 'options-page/breadcrumbs.php';

//for features
require $feature_path . 'features/tlp_reports.php';
require $feature_path . 'features/hide_page_title.php';
require $feature_path . 'features/breadcrumbs_popst_page.php';

?>