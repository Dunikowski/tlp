<?php
/**
 * Logic for use password from options page.  
 */

function password_post_check($post) {

    $post_categories=get_the_category($post->id); 
    /* get info about post to collect category 
    *  and compare with data from ACF option page below. Finaly this function should be retur true if post will have checked category in ACF option page
    */
     if ( have_rows( 'posts_pasword', 'options' ) ) :
         while ( have_rows( 'posts_pasword', 'options' ) ) :
            the_row();
            $password = get_sub_field( 'password', 'options' ) ;
            $protected_categories = get_sub_field( 'protected_categories', 'options' );
            
            if ( $protected_categories ) :
                 if ( $protected_categories ) :
                     foreach ( $protected_categories as $term ) :
                        /**
                         * value below you should use to make logic for display posts or template part with pass input.
                         */
                        if ($post_categories!=null && sizeof($post_categories)>0) {
                            foreach ($post_categories as $key => $postCat) {
                                if ($postCat->slug==$term->slug) {
                                    
                                    if(isset($_COOKIE['members_login'])) {
                                        if ($_COOKIE['members_login']==$password) {
                                            $_SESSION["passwordStatus"]='1';
                                            return true;
                                        }else {
                                            if (isset($_SESSION['members_login'])) {
                                                if ($_SESSION['members_login']==$password) {
                                                    $_SESSION["passwordStatus"]='1';
                                                    return true;
                                                }else {
                                                    return false;
                                                }
                                            }else
                                                return false;
                                        }
                                    }elseif (isset($_SESSION['members_login'])) {
                                        if ($_SESSION['members_login']==$password) {
                                            $_SESSION["passwordStatus"]='1';
                                            return true;
                                        }else {
                                            return false;
                                        }
                                    }else {
                                        return false;
                                    }
                                }
                            }
                        }
                        ?>
                        
                <?php
                     endforeach;
                 endif;
             endif;
         endwhile;
     endif;
    return true;
}
?>