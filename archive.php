<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 

/**
 * cumulus_before_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_content');
/**
 * cumulus_before_archive_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_archive_content');
?>
	<section id="primary" class="content-area col-sm-12 col-lg-8 archive">
		<div id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php

				if (function_exists('display_breadcrumbs') && get_field( 'bradcrumbs', 'options' )) {
					// above will be use breadcrumbs with information in else below.
				}
				else {
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				}

				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', get_post_format() );
				get_template_part( 'template-parts/category', get_post_format() );

			endwhile;

			// the_posts_navigation();
			cumulus_numeric_posts_nav();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
