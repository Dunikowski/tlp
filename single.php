<?php



/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

 if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php
/**
 * cumulus_before_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_content');
/**
 * cumulus_before_single_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_single_content');

?>

	<section id="primary" class="content-area col-sm-12 col-lg-8 post">
		<div id="main" class="site-main" role="main">

		<?php
		
		while ( have_posts() ) : the_post();

		if (function_exists('password_post_check')) {
			if (password_post_check($post) == true) {
				get_template_part( 'template-parts/content', get_post_format() );
			}else {
				get_template_part( 'template-parts/pass', get_post_format() ); 
			}
		}
		else {
			get_template_part( 'template-parts/content', get_post_format() );
		}	

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
		endwhile; // End of the loop.
		?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();