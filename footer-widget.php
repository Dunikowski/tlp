<?php

if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) {?>
        <div id="footer-widget" class="row m-0 footer-widget <?php if(!is_theme_preset_active()){ echo 'bg-light'; } ?>">
            <div class="container-tlp">
                <div class="row">
                    <?php if ( is_active_sidebar( 'footer-1' )) : ?>
                        <div class="col-12 col-md-6 col-lg-4 first-col"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-2' )) : ?>
                        <div class="col-12 col-md-6 col-lg-4 second-col"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-3' )) : ?>
                        <div class="col-12 col-md-6 col-lg-4 third-col"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="footer-social-menu">
            <?php if ( is_active_sidebar( 'footer-bar' )) : ?>
                <div class="footer-social-menu__content container-tlp"><?php dynamic_sidebar( 'footer-bar' ); ?></div>
            <?php endif; ?>
        </div>

<?php }