<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header();

/**
 * cumulus_before_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_content');
/**
 * cumulus_before_single_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_index_content');
?>

<?php
/**
 * hide becouse client want change view
 */
// if (is_home() && ! is_front_page()) {
// 	$new_class = 'post-grid';

// 	echo '<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-12 index">';

// }
// else {
// 	$new_class = '';

// 	echo '<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-8 index">';

// }
?>

<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-8 index">
	<div id="main" class="site-main <?php echo($new_class); ?>" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() && !get_field( 'bradcrumbs', 'options' )) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
		<?php endif; // else is not needed becouse h1 will display by breadcrub

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// if (is_home() && ! is_front_page()) :
				// get_template_part( 'template-parts/news', get_post_format() );
				// else :
				// 	get_template_part( 'template-parts/content', get_post_format() );
				// endif;
				get_template_part( 'template-parts/category', get_post_format() );

			endwhile;

			// the_posts_navigation();
			cumulus_numeric_posts_nav();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
/**
 * hide becouse client want change view
 */
// if (is_home() && ! is_front_page()) {

// } else {
// 	get_sidebar();
// }
get_sidebar();
get_footer();
