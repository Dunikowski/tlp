<?php
/**
 * WP Bootstrap Starter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WP_Bootstrap_Starter
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'wp_bootstrap_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wp_bootstrap_starter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WP Bootstrap Starter, use a find and replace
	 * to change 'cumulus' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'cumulus', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'Cumulus' ),
        'strefa_czlonkowska' => esc_html__( 'Strefa członkowska', 'Cumulus' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wp_bootstrap_starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    function wp_boostrap_starter_add_editor_styles() {
        add_editor_style( 'custom-editor-style.css' );
    }
    add_action( 'admin_init', 'wp_boostrap_starter_add_editor_styles' );

    add_theme_support( 'align-wide' );

}
endif;
add_action( 'after_setup_theme', 'wp_bootstrap_starter_setup' );

/**
 * Add Welcome message to dashboard
 */
function wp_bootstrap_starter_reminder(){
        $theme_page_url = 'https://afterimagedesigns.com/cumulus/?dashboard=1';

            if(!get_option( 'triggered_welcomet')){
                $message = sprintf(__( 'Welcome to WP Bootstrap Starter Theme! Before diving in to your new theme, please visit the <a style="color: #fff; font-weight: bold;" href="%1$s" target="_blank">theme\'s</a> page for access to dozens of tips and in-depth tutorials.', 'cumulus' ),
                    esc_url( $theme_page_url )
                );

                printf(
                    '<div class="notice is-dismissible" style="background-color: #6C2EB9; color: #fff; border-left: none;">
                        <p>%1$s</p>
                    </div>',
                    $message
                );
                add_option( 'triggered_welcomet', '1', '', 'yes' );
            }

}
add_action( 'admin_notices', 'wp_bootstrap_starter_reminder' );
function register_session(){

    if(session_id() == '' || !isset($_SESSION)) {
        // session isn't started
        session_start();

    }
    if (isset( $_POST['members_zone'])) {
        # code...
        $_SESSION["members_login"] = $_POST['members_zone'];
        $_SESSION["passwordStatus"]='0';

    }
    if (isset( $_SESSION["members_login"])) {
        # code...
        if ( isset($_COOKIE['cookie_notice_accepted']) ) {
            if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) :
                setcookie('members_login', $_SESSION["members_login"],time() + (10 * 365 * 24 * 60 * 60), '/');
            endif;
        }
    }

}
add_action('init','register_session');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wp_bootstrap_starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wp_bootstrap_starter_content_width', 1170 );
}
add_action( 'after_setup_theme', 'wp_bootstrap_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wp_bootstrap_starter_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'cumulus' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'cumulus' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'cumulus' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'cumulus' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 2', 'cumulus' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here.', 'cumulus' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 3', 'cumulus' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here.', 'cumulus' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Top bar', 'cumulus' ),
        'id'            => 'top-bar',
        'before_widget' => '<div class="widget %2$s top-bar__social-media">',
        'after_widget'  => '</div>',
        'description'   => esc_html__( 'Add widgets here.', 'cumulus' ),
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer bar', 'Cumulus' ),
        'id'            => 'footer-bar',
        'before_widget' => '<div class="widget %2$s footer-bar">',
        'after_widget'  => '</div>',
        'description'   => esc_html__( 'Add widgets here.', 'Cumulus' ),
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'wp_bootstrap_starter_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function wp_bootstrap_starter_scripts() {
	// load bootstrap css
    if ( get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
        wp_enqueue_style( 'cumulus-bootstrap-css', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
    }
    wp_enqueue_style( 'cumulus-fontawesome-cdn', get_template_directory_uri() . '/inc/assets/css/fontawesome.min.css' );
	// load bootstrap css
	// load AItheme styles
	// load WP Bootstrap Starter styles
	wp_enqueue_style( 'cumulus-style', get_stylesheet_uri() );
    if(get_theme_mod( 'theme_option_setting' ) && get_theme_mod( 'theme_option_setting' ) !== 'default') {
        wp_enqueue_style( 'cumulus-'.get_theme_mod( 'theme_option_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/theme-option/'.get_theme_mod( 'theme_option_setting' ).'.css', false, '' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-lora') {
        wp_enqueue_style( 'cumulus-poppins-lora-font', 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-merriweather') {
        wp_enqueue_style( 'cumulus-montserrat-merriweather-font', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,400i,700,900|Montserrat:300,400,400i,500,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-poppins') {
        wp_enqueue_style( 'cumulus-poppins-font', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'roboto-roboto') {
        wp_enqueue_style( 'cumulus-roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'arbutusslab-opensans') {
        wp_enqueue_style( 'cumulus-arbutusslab-opensans-font', 'https://fonts.googleapis.com/css?family=Arbutus+Slab|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'oswald-muli') {
        wp_enqueue_style( 'cumulus-oswald-muli-font', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800|Oswald:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-opensans') {
        wp_enqueue_style( 'cumulus-montserrat-opensans-font', 'https://fonts.googleapis.com/css?family=Montserrat|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'robotoslab-roboto') {
        wp_enqueue_style( 'cumulus-robotoslab-roboto', 'https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:300,300i,400,400i,500,700,700i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) && get_theme_mod( 'preset_style_setting' ) !== 'default') {
        wp_enqueue_style( 'cumulus-'.get_theme_mod( 'preset_style_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/typography/'.get_theme_mod( 'preset_style_setting' ).'.css', false, '' );
    }
    //Color Scheme
    /*if(get_theme_mod( 'preset_color_scheme_setting' ) && get_theme_mod( 'preset_color_scheme_setting' ) !== 'default') {
        wp_enqueue_style( 'cumulus-'.get_theme_mod( 'preset_color_scheme_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/'.get_theme_mod( 'preset_color_scheme_setting' ).'.css', false, '' );
    }else {
        wp_enqueue_style( 'cumulus-default', get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/blue.css', false, '' );
    }*/

	wp_enqueue_script('jquery');

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'html5hiv',get_template_directory_uri().'/inc/assets/js/html5.js', array(), '3.7.0', false );
    wp_script_add_data( 'html5hiv', 'conditional', 'lt IE 9' );

	// load bootstrap js
    // if ( get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
    //      wp_enqueue_script('cumulus-popper', get_template_directory_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
    //      wp_enqueue_script('cumulus-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
    //  }
     //else {
    //    wp_enqueue_script('cumulus-popper', get_template_directory_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
       //wp_enqueue_script('cumulus-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
    //}

    wp_enqueue_script('cumulus-themejs', get_template_directory_uri() . '/inc/assets/js/theme-script.min.js', array(), '', true );
	wp_enqueue_script( 'cumulus-skip-link-focus-fix', get_template_directory_uri() . '/inc/assets/js/skip-link-focus-fix.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    wp_enqueue_script('wp-tpl-slick-js', get_template_directory_uri() . '/inc/assets/js/slick.min.js', array('jquery'), '', true );
    wp_enqueue_script('wp-tpl-main-js', get_template_directory_uri() . '/inc/assets/js/main.js', array('jquery'), '', true );

}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_starter_scripts' );


function admin_style() {
    wp_enqueue_style('admin-styles-override', get_template_directory_uri().'/inc/assets/css/admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');
/**
 * Add Preload for CDN scripts and stylesheet
 */
function wp_bootstrap_starter_preload( $hints, $relation_type ){
    if ( 'preconnect' === $relation_type && get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
        $hints[] = [
            'href'        => 'https://cdn.jsdelivr.net/',
            'crossorigin' => 'anonymous',
        ];
        $hints[] = [
            'href'        => 'https://use.fontawesome.com/',
            'crossorigin' => 'anonymous',
        ];
    }
    return $hints;
}

add_filter( 'wp_resource_hints', 'wp_bootstrap_starter_preload', 10, 2 );



function wp_bootstrap_starter_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( home_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3 hide-br">' . __( "Aby zoabaczyć treść tej strony wprowadź hasło poniżej:", "cumulus" ) . '</div>
    <div class="form-group form-inline member-zone-form__form hide-br">
    <!--<label for="' . $label . '" class="mr-2">' . __( "Hasło:", "cumulus" ) . ' </label>-->
    <input name="post_password" id="' . $label . '" type="password" placeholder="' . __( "Hasło", "cumulus" ) . '" class="form-control mr-2 member-zone-form__input">
    <input type="submit" name="Submit" value="' . esc_attr__( "Zatwierdź", "cumulus" ) . '" class="btn btn-primary member-zone-form__button "/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'wp_bootstrap_starter_password_form' );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load plugin compatibility file.
 */
require get_template_directory() . '/inc/plugin-compatibility/plugin-compatibility.php';

/**
 * Most element below use ACF therefore for some debug issue is good add check ACF exist class before use it.
 *
 */
if( class_exists('ACF') ) :
    /**
     * Implement the ACF options and ACF features.
     */
    require get_template_directory() . '/inc/acf-config/acf-include.php';

    /**
     * Implement the adds logic. Setting should be added into database or in acf-config folder like new feature.
     */
    require get_template_directory() . '/inc/adds.php';

    /**
     * Implement the password logic. Setting should be added into database or in acf-config folder like new feature.
     */
    require get_template_directory() . '/inc/password_protect.php';

    /*
    * Implement the breadcrumbs logic. Setting should be added into database or in acf-config folder like new feature.
    */
    require get_template_directory() . '/inc/breadcrumbs.php';

    /*
    * Implement the acf additional code in header and footer. Setting should be added into database or in acf-config folder like new feature.
    */
    require get_template_directory() . '/inc/header_footer_acf_hoohs.php';

endif;

// do_action('cumulus_before_single_content');

/**
 * Load custom WordPress nav walker.
 */
if ( ! class_exists( 'wp_bootstrap_navwalker' )) {
    require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');
}


/**
 *  Functions for template
 */

 function get_date_top_bar() {

    $day = date('d');
    $day_of_the_week = date('l');
    $month = date('n');
    $year = date('Y');

    $months = array(1 => 'stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia');
    $days_of_the_week  = array('Monday' => 'Poniedziałek', 'Tuesday' => 'Wtorek', 'Wednesday' => 'Środa', 'Thursday' => 'Czwartek', 'Friday' => 'Piątek', 'Saturday' => 'Sobota', 'Sunday' => 'Niedziela');

    echo $days_of_the_week[$day_of_the_week ].", ".$day." ".$months[$month]." ".$year;
 }


 add_filter( 'wp_nav_menu_items', 'include_second__menu_navbar_icon', 10, 2 );
function include_second__menu_navbar_icon( $nav_items, $args ) {

    if ( $args->theme_location === 'strefa_czlonkowska' ) {

        $nav_items ='<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24.375" height="30" viewBox="0 0 24.375 30">
                        <path id="LOCK" d="M-1515.7-1498.682h-21.563a1.406,1.406,0,0,1-1.406-1.406v-12.188a1.406,1.406,0,0,1,1.406-1.406h2.344v-6.563a8.437,8.437,0,0,1,8.438-8.438,8.437,8.437,0,0,1,8.438,8.438v6.563h2.344a1.406,1.406,0,0,1,1.406,1.406v12.188A1.406,1.406,0,0,1-1515.7-1498.682Zm-12.188-6.953v2.734a1.406,1.406,0,0,0,1.406,1.407,1.407,1.407,0,0,0,1.406-1.407v-2.734a2.8,2.8,0,0,0,1.406-2.422,2.812,2.812,0,0,0-2.812-2.813,2.812,2.812,0,0,0-2.812,2.813A2.8,2.8,0,0,0-1527.885-1505.635Zm7.031-14.61a5.625,5.625,0,0,0-5.625-5.625,5.625,5.625,0,0,0-5.625,5.625v6.563h11.25Z" transform="translate(1538.667 1528.682)" fill="#122845" fill-rule="evenodd"/>
                    </svg>'. $nav_items;
    }

    return $nav_items;
}

function companyLogos($atts) {

    ob_start();

    $data = [];
    $data[ 'repeater'] = false;
    $data['carousel_setings'] = false;

    if(  class_exists('ACF') ) {

        if($atts['key'] && get_field($atts['key'],'option')) {

            $data[ 'repeater'] = get_field($atts['key'],'option');
            $carousel_setings = get_field($atts['key'].'_carousel_setings','option');

            if($carousel_setings) {

                $data['carousel_setings'] = json_encode($carousel_setings);
            }
        }
    }

	get_template_part( 'template-parts/carousel-theme','', $data);

    return ob_get_clean();
}

add_shortcode( 'company_logos', 'companyLogos' );


function addModal() {

    if(class_exists('ACF')) {

        $options = get_field('field_60ab8b19c2d0c','option');
    }

    if($options) {
        return get_template_part( 'template-parts/modal','',$options);

    } else {
        return '';
    }

}

add_action('after_footer_bar','addModal');

//numeric pagination
function cumulus_numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="posts-pagination"><ul>' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul></div>' . "\n";
}

add_shortcode('tlp_logos', 'cs_display_logos_in_grid');

function cs_display_logos_in_grid($atts) {

    ob_start();
    $data = $atts;
    get_template_part( 'template-parts/logos_in_grid', '' ,$data);

    return ob_get_clean();

}

/**
 * Recent post
 */
add_shortcode('recentposts', 'cs_recent_posts');

function cs_recent_posts($atts) {

    ob_start();
    $data = $atts;
    get_template_part( 'template-parts/recent_post_widget', '' ,$data);

    return ob_get_clean();

}

function cs_theme_custom_upload_mimes( $existing_mimes ) {
    // Add webm to the list of mime types.
    $existing_mimes['webm'] = 'video/webm';
    $existing_mimes['ai'] = 'ai/illustrator';
    // Return the array back to the function with our added mime type.
    return $existing_mimes;
}
add_filter( 'mime_types', 'cs_theme_custom_upload_mimes' );


/**
 * Posts of categories
 */

function cumulusDisplayCategories($atts) {

    $a = shortcode_atts( array(
		'style' => '1',
	), $atts );
    // var_dump($a['style']);

    if(isset($atts['categories']) && strlen($atts['categories']) > 0) {
        ob_start();
        global $paged;
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $default_posts_per_page = get_option( 'posts_per_page' );

        if(isset($atts['posts_per_page']) && strlen($atts['posts_per_page']) > 0) {
            $default_posts_per_page = intval($atts['posts_per_page']);
        }

        $args = array(
            'posts_per_page' => $default_posts_per_page,
            'paged' => $paged,
            'category_name' =>  $atts['categories'],
            'orderby' => 'date',
            'order'   => 'DESC',
        );

        $category_posts = new WP_Query($args);

        if($category_posts->have_posts()) {

           $data['posts'] = $category_posts->posts;
           $data['max_title_length'] = 0;

           if(isset($atts['max_title_length']) && strlen($atts['max_title_length']) > 0) {
                $data['max_title_length'] = intval($atts['max_title_length']);
           }

           if($a['style'] == '1') {
                get_template_part( 'template-parts/category-default','', $data);
            }
            else {
                get_template_part( 'template-parts/category-different','', $data);
            }
            $big = 999999999;
            $pagination = '';
            $pagination .= paginate_links(
                array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '/page/%#%',
                    'current' => max(
                        1,
                        get_query_var('paged')
                    ),
                    'total' => $category_posts->max_num_pages,
                    'prev_text'    => __('« Poprzednia'),
                    'next_text'    => __('Nastepna »'),
                )
            );
            printf( '<div class="posts-pagination">%s</div>', $pagination);
        }

        wp_reset_postdata();

        return ob_get_clean();
    }

    $worning = '';

    if(is_user_logged_in()) {
        $worning = __('Nie dodałeś atrybutu categories do shortcode cs_display_categories, bądź jest on pusty.');
    } else {
        $worning = __('Brak danych do wyświetlenia.');
    }

  return $worning;
}
add_shortcode( 'cs_display_categories', 'cumulusDisplayCategories' );