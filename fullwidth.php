<?php
/**
* Template Name: Full Width
 */

get_header(); 

/**
 * cumulus_before_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_content');
/**
 * cumulus_before_single_content hook.
 *
 * @since 0.1
 */
do_action('cumulus_before_single_content');
?>

	<section id="primary" class="content-area col-sm-12">
		<div id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
