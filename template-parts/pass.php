<?php
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
    // session isn't started
}
if (isset( $_POST['members_zone'])) {
    # code...
    $_SESSION["members_login"] = $_POST['members_zone'];
    $_SESSION["passwordStatus"]='0';
    
}

?>
<header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header>
<div class="entry-content member-zone-form">
    <?php if(has_excerpt()):;?>
    <div class="member-zone-form__post-excerpt"><?php the_excerpt();?></div>
    <?php endif;?>
    <p class="member-zone-form__text"><?php _e('Ta treść jest chroniona hasłem. Aby ją zobaczyć, podaj hasło poniżej:'); ?></p>
    <form method="POST" action="" class="member-zone-form__form">
        <input type="password" id="members_zone" class="member-zone-form__input" name="members_zone" value="" placeholder="Hasło" required="">
        <?php if (isset($_SESSION["passwordStatus"]) && $_SESSION["passwordStatus"]=='0' ) : ?>
            <p class="error error-message"> <?php _e('Podane hasło jest niepoprawne.' , 'cumulus'); ?></p> 
        <?php endif; ?>
        <button type="submit" class="member-zone-form__button "><?php _e('Zatwierdź'); ?></button>
    </form>
</div>