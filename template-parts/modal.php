<div class="modal-wrapper">
    <input id="prodId" name="prodId" type="hidden" data-toggle="modal" data-target="#exampleModalLong">
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal__content">
                <?php if($args['heading']) : ;?>
                <div class="modal__header">
                    <span class="modal__title" id="exampleModalLongTitle"><?php _e($args['heading']);?></span> 
                </div>
                <?php endif;?>
                <?php if($args['content']) : ;?>
                    <div class="modal__body">
                    <?php echo $args['content'];?>
                    </div>
                <?php endif;?>
              
                    <div class="modal__wrapper-buttons">
                        <span class="close modal__button-close" data-dismiss="modal" aria-label="Close"><?php _e('Close');?></span>
                        <?php if($args['buttons']) : ;?>
                        <?php foreach ($args['buttons'] as $item): ;?>
													<?php if($item['link_blank']) : ;?>
														<a href="<?php  echo $item['link'];?>" class="modal__button-come-in" target="_blank" rel="noopener"><?php _e($item['button_name']);?></a>
														<?php else: ?> 
														<a href="<?php  echo $item['link'];?>" class="modal__button-come-in"><?php _e($item['button_name']);?></a>
													<?php endif;?>  
                        <?php endforeach ;?>
                        <?php endif;?>  
                    </div>
               
            </div>
        </div>
    </div>
</div>
