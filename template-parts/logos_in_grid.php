<?php 
    $logos_tyle = $args['type'] ?? 'tlp_partners_logo';

if( class_exists('ACF') ) :

if ( have_rows( $logos_tyle, 'options' ) ) : ?>
<div class="logo-grid row">
	<?php while ( have_rows( $logos_tyle, 'options' ) ) :
		the_row(); ?>
        <div class="logo-grid__item col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 ">
            <?php
            $img = get_sub_field( 'img', 'options' );
            if ( $img ) : 
            ( $url = get_sub_field( 'url', 'options' ) ) ? $action_hover = ' hover_true' : $action_hover = ' '; ?>
                <div class="frame <?php echo $action_hover ?>">
                    <figure>
                        <?php if ( $url = get_sub_field( 'url', 'options' ) ) : ?>
                            <a href="<?php echo esc_url( $url ); ?>" target="_blank" rel="noopener noreferrer">
                        <?php endif; ?>
                        <img src="<?php echo esc_url( $img['url'] ); ?>" alt="<?php echo esc_attr( $img['alt'] ); ?>" />
                        <?php if ( $url = get_sub_field( 'url', 'options' ) ) : ?>
                            </a>
                        <?php endif; ?>

                        <?php if ( $organization_name = get_sub_field( 'organization_name', 'options' ) ) : ?>
                            <figcaption class="sr-only"> <?php echo esc_html( $organization_name ); ?> </figcaption>
                        <?php endif; ?>
                    </figure>
                </div>
            <?php endif; ?>
        </div>
	<?php endwhile; 
    wp_reset_postdata();?>
</div>
<?php endif; ?>

<?php endif; ?>