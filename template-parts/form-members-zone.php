
<header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header>
<div class="entry-content member-zone-form">
    <p class="member-zone-form__text"><?php _e('Ta treść jest chroniona hasłem. Aby ją zobaczyć, podaj hasło poniżej:'); ?></p>
    <form method="GET" action="" class="member-zone-form__form">
        <input type="text" id="members_zone" class="member-zone-form__input" name="members_zone" value="" placeholder="Hasło" required="">
        <button type="submit" class="member-zone-form__button "><?php _e('Zatwierdź'); ?></button>
    </form>
</div>

