<?php if($args['posts']):;?>
<div class="selected-categories">
	<?php foreach ($args['posts'] as $item): ;?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('grid-news grid-news__category'); ?>>
			<div class="entry-meta">
				<?php
				echo get_the_date(get_option('date_format'), $item->ID);
				?>
			</div>
			<a href="<?php the_permalink( $item->ID);?>" rel="bookmark">
				<div class="post-thumbnail">
				<?php
					if(get_post_thumbnail_id($item->ID)) {
						echo wp_get_attachment_image(get_post_thumbnail_id($item->ID),'full',"",["class" => ""]);
					}
				?>
					<header class="entry-header">
						<h2 class="entry-title extra-bold">
						<?php
							echo $item->post_title;
						?>
						</h2>
					</header>
				</div>
			</a>
		<span class="add-height-30"></span>
	</article>
	<?php endforeach ;?>
</div>
<?php endif;?>



