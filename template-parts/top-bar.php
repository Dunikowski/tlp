<div class="top-bar">
	<div class="top-bar__wrapper">
		<div class="top-bar__date"><?php get_date_top_bar();?></div>
		<?php if ( is_active_sidebar( 'top-bar' )) dynamic_sidebar( 'top-bar' );?>
	</div>
</div>