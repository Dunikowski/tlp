<?php if($args['repeater'] && is_array($args['repeater'])): ;?>
	<div class="wp-cumulus-carousel js-wp-cumulus-carousel" data-carousel-setings='<?php echo ($args['carousel_setings'])?$args['carousel_setings']:'' ;?>'>
	<?php foreach ($args['repeater'] as $item): ;?>
		<div class="wp-cumulus-carousel__items">
			<?php if($item['url']): ;?>
				<a href="<?php echo $item['url'];?>">
			<?php endif;?>
					<img class="wp-cumulus-carousel__img" src="<?php echo $item['img']['sizes']['medium'];?>" alt="<?php echo $item['img']['alt'];?>"/>
			<?php if($item['url']): ;?>
				</a>
			<?php endif;?>
		</div>
		<?php endforeach;?>
	</div>
<?php endif;?>

