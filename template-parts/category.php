<?php
/**
 * Template part for displaying posts in category archive
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */


?>

<article id="post-<?php the_ID(); ?>" <?php post_class('grid-news grid-news__category'); ?>>
	<?php

	if ( 'post' === get_post_type() ) : ?>
	<div class="entry-meta">
		<?php 
		// the_date(); // sometimes return null whe get_the_date retur correct value.
		echo get_the_date();
		// wp_bootstrap_starter_posted_on(); all metadata is not rquire in this layout
		?>
	</div><!-- .entry-meta -->
	<?php
	endif; 
	?>
	<a href="<?php esc_url( the_permalink() ); ?>" rel="bookmark"> 
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		
            <header class="entry-header">
                <?php

                if ( is_single() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                    else :
                        the_title( '<h2 class="entry-title extra-bold">', '</h2>' );
                endif;

                ?>
            </header><!-- .entry-header -->
        </div>
    </a>
    <span class="add-height-30"></span>
</article><!-- #post-## -->
