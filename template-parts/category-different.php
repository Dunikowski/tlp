<?php if($args['posts']):;?>
	<article id="post-<?php the_ID(); ?>" class="post-grid selected-categories">
		<?php foreach ($args['posts'] as $item): ;?>
			<a href="<?php the_permalink( $item->ID);?>" class="selected-categories__link"> 
				<?php 
					if(get_post_thumbnail_id($item->ID)) {
						echo wp_get_attachment_image(get_post_thumbnail_id($item->ID),'medium_large',"",["class" => "selected-categories__img"]);
					}
				;?>
					<span class="selected-categories__title">
						<?php 
						if(strlen($item->post_title) > $args['max_title_length'] && $args['max_title_length'] !== 0) {
							echo substr($item->post_title,0,$args['max_title_length']).' ...';
						} else {
							echo $item->post_title;
						}
						?>
					</span>
				</a>								
		<?php endforeach ;?>
	</article>
<?php endif;?>



