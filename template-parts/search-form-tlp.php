<form role="search" method="get" class="js-search-form-cumulus search-form-cumulus" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-form-cumulus__content">
      <div class="search-form-cumulus__wrapper">
        <label for="search-field-cumulus"><?php _e('Szukaj','Cumulus');?></label>
        <input id="search-field-cumulus" type="search" class="search-field search-form-cumulus__input" name="s" title="<?php _ex( 'Search for:', 'label', 'cumulus' ); ?>">
        <svg class="search-svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
          <path id="Path_639" data-name="Path 639" d="M-1196.6-3130.612l-8.388-8.387a11.546,11.546,0,0,0,1.617-5.892,11.615,11.615,0,0,0-11.615-11.615,11.615,11.615,0,0,0-11.615,11.615,11.614,11.614,0,0,0,11.615,11.614,11.533,11.533,0,0,0,5.891-1.617l8.387,8.388Zm-18.385-5.568a8.711,8.711,0,0,1-8.711-8.711,8.712,8.712,0,0,1,8.711-8.711,8.712,8.712,0,0,1,8.711,8.711A8.711,8.711,0,0,1-1214.982-3136.18Z" transform="translate(1226.597 3156.506)" fill="#fff" fill-rule="evenodd"/>
        </svg>
      </div>
    </div>
    <div class="search-form-cumulus__button">
      <svg class="search-svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
        <path id="Path_639" data-name="Path 639" d="M-1196.6-3130.612l-8.388-8.387a11.546,11.546,0,0,0,1.617-5.892,11.615,11.615,0,0,0-11.615-11.615,11.615,11.615,0,0,0-11.615,11.615,11.614,11.614,0,0,0,11.615,11.614,11.533,11.533,0,0,0,5.891-1.617l8.387,8.388Zm-18.385-5.568a8.711,8.711,0,0,1-8.711-8.711,8.712,8.712,0,0,1,8.711-8.711,8.712,8.712,0,0,1,8.711,8.711A8.711,8.711,0,0,1-1214.982-3136.18Z" transform="translate(1226.597 3156.506)" fill="#fff" fill-rule="evenodd"/>
      </svg>
      <svg class="close-svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
        <path id="Path_571" data-name="Path 571" d="M3074.35-3318.741l-2.65-2.65-7.35,7.35-7.35-7.35-2.65,2.65,7.35,7.35-7.35,7.35,2.65,2.65,7.35-7.35,7.35,7.35,2.65-2.65-7.35-7.35Z" transform="translate(-3054.35 3321.392)"/>
      </svg>
    </div>
</form>



