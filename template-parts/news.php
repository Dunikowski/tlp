<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */


?>

<article id="post-<?php the_ID(); ?>" <?php post_class('grid-news'); ?>>
	<?php

	if ( 'post' === get_post_type() ) : ?>
	<div class="entry-meta">
		<?php 
		// the_date(); // sometimes return null whe get_the_date retur correct value.
		echo get_the_date();
		// wp_bootstrap_starter_posted_on(); all metadata is not rquire in this layout
		?>
	</div><!-- .entry-meta -->
	<?php
	endif; ?>
	<a href="<?php esc_url( the_permalink() ); ?>" > 
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
		<header class="entry-header">
			<?php

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title extra-bold">', '</h2>' );
			endif;

			?>
		</header><!-- .entry-header -->
	</a>
	<!-- <div class="entry-content"> -->
		<?php
        // if ( is_single() ) :
		// 	the_content();
        // else :
        //     the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'cumulus' ) );
        // endif;

			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cumulus' ),
			// 	'after'  => '</div>',
			// ) );
		?>
	<!-- </div> -->
	<!-- .entry-content -->

	<footer class="entry-footer">
		<?php 
		the_category();
		//wp_bootstrap_starter_entry_footer(); // all metadata is not rquire in this layout ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
