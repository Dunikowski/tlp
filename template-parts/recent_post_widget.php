<div class="recent-posts-widget">
    <div class="recent-posts-widget__header">
    <h2 class="entry-title overlap__heading"><?php _e('Najnowsze wpisy' , 'cumulus'); ?></h2>
    <span class="recent-posts-widget__accent primary-border-accent"></span>
    </div>
    
    <ul class="recent-posts-widget">
    
    <?php 
    
    // Define our WP Query Parameters

    $query_args = shortcode_atts( array (
        'post_status'       => 'publish',
        'orderby'           => 'date',
        'order'             => 'DESC',
        'category_name'		=> $args['categories'] ?? '',
        'posts_per_page'    => $args['items'] ?? '5'
    ), $args , 'recentposts');


    $the_query = new WP_Query( $query_args ); 
    // Start our WP Query
    while ($the_query -> have_posts()) : $the_query -> the_post(); 
    // Display the Post Title with Hyperlink
    ?>
    <article class="recent-posts-widget__article">
        <div class="post-thumbnail">
                <?php the_post_thumbnail(); ?>
        </div>
        <h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
        <?php the_category(); ?>
    </article>
    <?php 
    // Repeat the process and reset once it hits the limit
    endwhile;
    wp_reset_postdata();
    ?>
    </ul>
</div>