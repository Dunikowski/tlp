<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
        <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Szukaj  &hellip;', 'placeholder', 'cumulus' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Szukaj:', 'label', 'cumulus' ); ?>">
    </label>
    <button type="submit" class="search-submit btn btn-default" value="<?php echo esc_attr_x( 'Szukaj', 'submit button', 'cumulus' ); ?>"><span class="search-btn-icon"></span></button>
</form>



