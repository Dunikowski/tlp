const generateBackToTop = ($) => $(() => {
	'use strict';

    
	if ( 'querySelector' in document && 'addEventListener' in window ) {

		var goTopBtn = document.querySelector( '.generate-back-to-top' );

		var trackScroll = function() {
			var scrolled = window.pageYOffset;
			var coords = goTopBtn.getAttribute( 'data-start-scroll' ) ;

			if ( scrolled > coords ) {
				goTopBtn.style.opacity = '1';
				goTopBtn.style.visibility = 'visible';
			}

			if (scrolled < coords) {
				goTopBtn.style.opacity = '0';
				goTopBtn.style.visibility = 'hidden';
			}
		};

		// Function to animate the scroll
		var smoothScroll = function (anchor, duration) {
			// Calculate how far and how fast to scroll
			var startLocation = window.pageYOffset;
			var endLocation = document.body.offsetTop;
			var distance = endLocation - startLocation;
			var increments = distance/(duration/16);
			var stopAnimation;

			// Scroll the page by an increment, and check if it's time to stop
			var animateScroll = function () {
				window.scrollBy(0, increments);
				stopAnimation();
			};

			// Stop animation when you reach the anchor OR the top of the page
			stopAnimation = function () {
				var travelled = window.pageYOffset;
				if ( travelled <= (endLocation || 0) ) {
					clearInterval(runAnimation);
					document.activeElement.blur();
				}
			};

			// Loop the animation function
			var runAnimation = setInterval(animateScroll, 16);
		};

		if ( goTopBtn ) {
			// Show the button when scrolling down.
			window.addEventListener( 'scroll', trackScroll );

			// Scroll back to top when clicked.
			goTopBtn.addEventListener( 'click', function( e ) {
				e.preventDefault();
				smoothScroll( document.body, goTopBtn.getAttribute( 'data-scroll-speed' ) || 400 );
			}, false );
		}
	}
 }); 


 const _calculate = ($) => {
  const navbar = $('.container-header > .navbar');
  if(navbar == 'undefined' || navbar.length < 1) return;
  const width = navbar.width();
  const childs = navbar.find('>*');
 
  let _sum = 0;
  let _index = null;

  if(childs.length > 0) {

    childs.each(function(index) {
			if($(this)[0].className.indexOf('navbar-toggler') !== -1) return;
      let i = 0;
			i = $(this).outerWidth();

      if(_sum !== 0) {
        _sum += i;
        if(_sum > width) {
         
          _index = index;
          return false;
        }
      } else {
        _sum += i;
      }
    });

    childs.each(function(index) {

      $(this).removeClass('flex-basic-siblings');

      if(_index == index + 1) {
        $(this).addClass('flex-basic-siblings');
      } 
    })
  }
 }

const isSpaceInNavbarBar = ($) =>  $(() => {
	'use strict';
	
  _calculate($);
	let flag = true;

  $(window).on('resize', function(){
  
    if ($(this).width() > 1200 && flag) { 
      flag = false;
      _calculate($);
      setTimeout(() => {
        flag = true;
      },300)
     } else {
       return false;
     }
  });
})


const callAutoModal = ($) => $(() => {

	$('.js-modal-popup').on('click', function(e){

		e.preventDefault();
		const i = $("#prodId");
		if(i.length > 0) {
			i.trigger('click');
		} else {
			return;
		}
	})

});

 const stickyNavBar = () => {

	 const element = document.querySelector('header > .container-header');
	 const elementPosition = element.offsetTop;
	 
	 window.addEventListener("scroll", () => {

		const window_position = Math.floor(window.scrollY);		 
		const classList = element.classList;

		if(elementPosition + element.offsetHeight < window_position) {

			if(!classList.contains('navbar-fixed-top')) classList.add('navbar-fixed-top');

		} else {

			if(classList.contains('navbar-fixed-top')) classList.remove('navbar-fixed-top');
		}
	})
 }

export default { 
  generateBackToTop,  
  isSpaceInNavbarBar,
	callAutoModal,
	stickyNavBar
}