const menuSearchForm = ($) =>  $(() => {
    const form = $('.js-search-form-cumulus');
    const button = form.find('.search-form-cumulus__button');
    const send = form.find('.search-form-cumulus__content .search-svg');

    if(button == 'undefined' || button.length < 1) return;

    button.on('click', (event) => {
        event.stopPropagation();
        $(event.target).closest('.search-form-cumulus').toggleClass('search-form-cumulus--active');
        form.find('.search-form-cumulus__input').val('');
    }); 

    if(send == 'undefined' || send.length < 1) return;
   
    send.on('click', (event) => {

        event.stopPropagation();
        $(event.target)
        .closest('.search-form-cumulus')
        .toggleClass('search-form-cumulus--active')
        .submit();
    }); 
})

export default { menuSearchForm };