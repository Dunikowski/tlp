import '../scss/style.scss';
import mainNav from './bootstrap/collapse';
import dropdownMenu from './bootstrap/dropdown';
import footerModal from './bootstrap/modal';
import searchForm from './search-form-tlp';
import callSlickCarousels  from './carousel-setings';
import tools from './tools';
import 'particles.js';

/*
 *   Call navBar
*/

mainNav;
dropdownMenu;
footerModal;

/*
* Call Serach Form navbar
 */

searchForm.menuSearchForm(jQuery);

/*
* Call Slick Carousels
 */

callSlickCarousels.frontPageGallery(jQuery);

callSlickCarousels.carouselsLogo(jQuery);


/**
 * Call tools functions
 */

tools.generateBackToTop(jQuery);

tools.isSpaceInNavbarBar(jQuery);

tools.callAutoModal(jQuery);

tools.stickyNavBar();

/**
 * get particles
 */

 particlesJS.load('particles-js', '/wp-content/themes/tlp/src/js/particles.json', function() {
    // console.log('callback - particles.js config loaded');
  });


function getResolution() {
  alert("Your screen resolution is: " + screen.width + "x" + screen.height);
}
// getResolution()