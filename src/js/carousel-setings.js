
const carouselInit = (_this = null) => {

	if(_this === null) return;

	const _data = _this.data('carouselSetings');
	let carousel_setings = {};
	carousel_setings.speed = 300;
	carousel_setings.slidesToShow = 3
	carousel_setings.slidesToScroll = 1;
	carousel_setings.swipeToSlide = true;
	carousel_setings.lazyLoad = 'ondemand';
	carousel_setings.prevArrow = '<div class="slick-prev slick-arrow"></div>';
	carousel_setings.nextArrow = '<div class="slick-next slick-arrow"></div>';

	if(_data !== '') {

			for (const [key, value] of Object.entries(_data)) {

					if(typeof value === "boolean") {
							carousel_setings[key] = value;

					} else if(!Array.isArray(value)) {
							carousel_setings[key] = parseInt(value);

					} else if(Array.isArray(value)) {

							carousel_setings[key] = value.map(item => {
									let res = {};
									res.breakpoint = parseInt(item.brekpoint);
									res.settings = {};
									for (const [key, value] of Object.entries(item.settings)) {
											if(isNaN(parseInt(value))) {
													res.settings[key] = value;
											} else {
													res.settings[key] = parseInt(value);
											}
									}

									return res;
							})
					}
			}
	}

	if(_this) {
			_this.slick(carousel_setings);
	}
}

const carouselsLogo = ($) =>  $(() => {

    const instances_carousels = $('.js-wp-cumulus-carousel');

    instances_carousels.each(function(){
        carouselInit($(this));
    })
});

const gallery_propertys = {
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    lazyLoad: 'ondemand',
    infinite: true,
    prevArrow:"<div class='slick-prev slick-arrow'></div>",
    nextArrow:"<div class='slick-next slick-arrow'></div>",
    responsive: [
    {
        breakpoint: 1024,
        settings: {
        slidesToShow: 2,
        slidesToScroll: 1
        }
    },
    {
        breakpoint: 480,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1
        }
    }
    ]
}

const frontPageGallery = ($) =>  $(() => {

    const front_page_gallery = $('.js-front-page-gallery');

    if(front_page_gallery !== 'undefined' || front_page_gallery !== 0) {

        front_page_gallery.slick(gallery_propertys);
    }

});

export default {
    carouselsLogo,
    frontPageGallery
};