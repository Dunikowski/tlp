<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container-tlp">
            <div class="site-info">
                &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
                <span class="sep"> | </span>
                <a class="credits" href="https://agencjacumulus.pl/" target="_blank" title="Agencja Reklamowa Cumulus" alt="Agencja Reklamowa Cumulus"><?php echo esc_html__('Projekt i wykonanie: Agencja Reklamowa Cumulus','Cumulus'); ?></a>

            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->
<?php
echo apply_filters(
			'generate_back_to_top_output',
			sprintf(
				'<a title="%1$s" aria-label="%1$s" rel="nofollow" href="#" class="generate-back-to-top" style="opacity:0;visibility:hidden;" data-scroll-speed="%2$s" data-start-scroll="%3$s">
					
				</a>',
				esc_attr__( 'Scroll back to top', 'generatepress' ),
				absint( 400 ),
				absint( 300),
				esc_attr( 'fa-angle-up')
				// generate_get_svg_icon( 'arrow-up' )
			)
		);
;?>
<?php 

do_action( 'after_footer_bar' );

 ?>
<?php wp_footer(); ?>
</body>
</html>